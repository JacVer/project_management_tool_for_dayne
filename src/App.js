import React from 'react';
import DrawBoard from './components/DrawBoard'
import 'semantic-ui-css/semantic.min.css'


class App extends React.Component {

  render() {
    return (
      <div>
        <h1>Hello world?</h1>
        <DrawBoard width={500} height={500}/>
      </div>
    );
  }
}

export default App;
