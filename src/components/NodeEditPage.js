import React from 'react';
import PropTypes from 'prop-types';
import { Button, Input, Segment } from 'semantic-ui-react';


export default class NodeEditPage extends React.Component {
  constructor(props) {
    super(props)
    const nodeData = props.getNodeData()

    this.state = {
      title: nodeData.title,
      description: nodeData.description
    }
  }

  onChangeTitle = (e) => {
    const newTitle = e.currentTarget.value
    this.setState((state) => { state.title = newTitle; return state})
  }

  onChangeDescription = (e) => {
    const newDescription = e.currentTarget.value
    this.setState((state) => { state.description = newDescription; return state})
  }

  componentWillUnmount = () => {
    this.props.updateNodeData(this.state.title, this.state.description)
  }

  render() {
    return (
      <Segment
        style={{
          left: this.props.x, 
          top: this.props.y, 
          position: 'fixed', 
          zIndex: 1
        }}
      >
        <div>
          <h2>Edit node</h2>
          <Input 
            placeholder="Title"
            value={this.state.title}
            onChange={this.onChangeTitle}
          />
          <Input
            placeholder="Description"
            value={this.state.description}
            onChange={this.onChangeDescription}
          />
        </div>
      </Segment>
    )
  }
}



NodeEditPage.propTypes = {
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  getNodeData: PropTypes.func.isRequired,
  updateNodeData: PropTypes.func.isRequired
}