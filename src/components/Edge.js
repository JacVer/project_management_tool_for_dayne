import React from "react";
import PropTypes from "prop-types";

export default class Edge extends React.Component {

  constructor(props) {
    super(props)
    this.normalStrokeWidth = 2
    this.strokeWidthFactor = 3

    const getEdgeBaseStrokeWidth = () => {
      if(props.isBeingDrawn) return this.normalStrokeWidth * this.strokeWidthFactor
      else return this.normalStrokeWidth
    }

    this.state = {
      strokeWidth: getEdgeBaseStrokeWidth()
    }
  }

  getLineAngle = (u1, u2) => {
    if(u1 < 0) return Math.atan(u2 / u1) + Math.PI
    else return Math.atan(u2 / u1)
  }

  getEdgeSupportVector = () => {
    return [this.props.pointerX - this.props.sourceX, this.props.pointerY - this.props.sourceY]
  }

  getArrowHeadPoints = (pointerX, pointerY, supportVectorX, supportVectorY) => {
    
    const A = [pointerX, pointerY]

    // ... get line vector angle and the line base (1) in terms of the global base (0)
    const lineAngle = this.getLineAngle(supportVectorX, supportVectorY)
    const x1 = [Math.cos(lineAngle), Math.sin(lineAngle)]
    const y1 = [-1 * Math.sin(lineAngle), Math.cos(lineAngle)]

    // ... get arrow vectors in the line base (1)
    const AC = [-1 * this.props.arrowHeadHeight, this.props.arrowHeadWidth / 2]
    const AB = [-1 * this.props.arrowHeadHeight, - this.props.arrowHeadWidth / 2]
    // ... get global arrow point coordinates (0)
    const C = [A[0] + AC[0] * x1[0] + AC[1] * y1[0], A[1] + AC[0] * x1[1] + AC[1] * y1[1]]
    const B = [A[0] + AB[0] * x1[0] + AB[1] * y1[0], A[1] + AB[0] * x1[1] + AB[1] * y1[1]]

    return [A, B, C]
  }

  onMouseEnterComponent = () => {
    if(!this.props.isBeingDrawn) {
      this.setState((state) => {
        state.strokeWidth = this.normalStrokeWidth * this.strokeWidthFactor
        return state
      })
      this.props.updateEdgeMouseOver(true)
    }
  }

  onMouseLeaveComponent = () => {
    if(!this.props.isBeingDrawn) {
      this.setState((state) => {
        state.strokeWidth = this.normalStrokeWidth
        return state
      })
      this.props.updateEdgeMouseOver(false)
    }
  }

  render() {

    const edgeSupportVector = this.getEdgeSupportVector()
    const arrowHeadPoints = this.getArrowHeadPoints(
      this.props.pointerX, 
      this.props.pointerY,
      edgeSupportVector[0],
      edgeSupportVector[1]
    )

    return (
      <g
        onMouseEnter={this.onMouseEnterComponent}
        onMouseLeave={this.onMouseLeaveComponent}
      >
        <line 
          x1={this.props.sourceX} 
          y1={this.props.sourceY} 
          x2={this.props.pointerX} 
          y2={this.props.pointerY} 
          stroke={this.props.edgeColor}
          strokeWidth={this.state.strokeWidth}
        />
        <polygon
          points={arrowHeadPoints.reduce((a1, a2) => a1.toString() + " " + a2.toString())}
          fill={this.props.edgeColor}
          stroke={this.props.edgeColor}
          strokeWidth={this.state.strokeWidth}
        />
      </g>
    )
  }
}

Edge.propTypes = {
  sourceX: PropTypes.number.isRequired,
  sourceY: PropTypes.number.isRequired,
  pointerX: PropTypes.number.isRequired,
  pointerY: PropTypes.number.isRequired,
  arrowHeadWidth: PropTypes.number.isRequired,
  arrowHeadHeight: PropTypes.number.isRequired,
  edgeColor: PropTypes.string.isRequired,
  isBeingDrawn: PropTypes.bool.isRequired,
  updateEdgeMouseOver: PropTypes.func.isRequired
}