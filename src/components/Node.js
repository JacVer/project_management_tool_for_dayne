import React from 'react';
import PropTypes from 'prop-types';
import { Portal } from 'semantic-ui-react';
import * as d3 from 'd3';

import NodeEditPage from './NodeEditPage'


export default class Node extends React.Component {
  constructor(props) {
    super(props)

    this.circleColour = "lightgrey"
    this.strokeColour = "blue"

    this.circleRadiusDefault = props.r

    this.innerCircleStrokeWidthWhenmouseOver = 3
    this.backgroundCircleDefaultRadius = props.r
    this.backgroundCircleRadiusWhenSelected = props.r * 1.15
    this.backgroundCircleRadiusWhenSelectedAndMouseOver = props.r * 1.3

    this.nodePageRightShift = 1.2 * this.backgroundCircleRadiusWhenSelected

    this.state = {
      isAllowedToMove: false,
      x: props.x,
      y: props.y,
      mouseOverComponent: true,
      mouseOverStroke: false,
      openNodePage: false
    }
  }

  updateCoordinates = (x, y) => {
    this.props.updateCoordinates(x, y)
    this.setState((state) => {
      state.x = x
      state.y = y
      return state
    })
  }

  // ... Mouse event functions 

  onMouseEnterComponent = (e) => {
    this.props.setEdgeEndNodeId()
    this.props.updateNodeMouseOver(true)
    this.setState(state => {state.mouseOverComponent = true; return state})
  }

  onMouseLeaveComponent = (e) => {
    this.props.resetEdgeEndNodeId()
    this.props.updateNodeMouseOver(false)
    this.setState(state => {state.mouseOverComponent = false; return state})
  }

  onMouseDownCircle = (e) => {
    // TODO : refactor this function to be a) more general b) better named 'updateSelected' ? should it better be 'updateMouseDown' ?
    this.props.setSelected()
    this.setState((state) => {state.isAllowedToMove = true; return state})
  }

  onMouseUpCircle = (e) => {
    this.setState((state) => {state.isAllowedToMove = false; return state})
  }

  onMouseMoveCircle = (e) => {
    if (this.props.selected && this.state.isAllowedToMove) {
      const coord = d3.pointer(e)
      this.updateCoordinates(coord[0], coord[1])
    }
  }

  onMouseOverStroke = (e) => {
    this.setState((state) => {state.mouseOverStroke = true; return state})
  }

  onMouseLeaveCircle = (e) => {
    this.setState((state) => { 
      state.isAllowedToMove = false
      return state
    })
  }

  onMouseLeaveStroke = (e) => {
    this.setState((state) => { state.mouseOverStroke = false; return state})
  }

  onDoubleClickComponent = (e) => {
    const previousOpenNodePage = this.state.openNodePage
    this.setState(state => {state.openNodePage = !previousOpenNodePage; return state})
  }

  // ... Node page functions

  handleNodePageClose = (e) => {
    this.setState(state => {state.openNodePage = false; return state})
  }

  render() {
    const getBackgroundCircleRadius = () => {
      if (this.state.mouseOverStroke && this.props.selected) {return this.backgroundCircleRadiusWhenSelectedAndMouseOver}
      else if (this.props.selected) {return this.backgroundCircleRadiusWhenSelected}
      else {return this.backgroundCircleDefaultRadius}
    }

    const getInnerCircleStrokeWidth = () => {
      if(!this.props.selected && this.state.mouseOverComponent) return this.innerCircleStrokeWidthWhenmouseOver
      else return 0
    }

    const getNodePage = () => {
      if(this.state.openNodePage) {
        return (
          // TODO : causes an error when closing 
          <Portal
            onClose={this.handleNodePageClose}
            open={true}
          >
            <NodeEditPage
              x={this.state.x + this.nodePageRightShift}
              y={this.state.y}
              getNodeData={this.props.getNodeData}
              updateNodeData={this.props.updateNodeData}
            />
          </Portal>
        )
      }
    }

    return (
      <g>
        <g
          onMouseEnter={this.onMouseEnterComponent}
          onMouseLeave={this.onMouseLeaveComponent}
          onDoubleClick={this.onDoubleClickComponent}
        >
          <circle 
            cx={this.state.x}
            cy={this.state.y}
            r={getBackgroundCircleRadius()}
            fill={this.strokeColour}
            onMouseOver={this.onMouseOverStroke}
            onMouseLeave={this.onMouseLeaveStroke}
            onMouseDown={this.props.setEdgeStartNodeId}
          />,
          <circle
            cx={this.state.x}
            cy={this.state.y}
            r={this.circleRadiusDefault}
            fill={this.circleColour}
            stroke="grey"
            strokeWidth={getInnerCircleStrokeWidth()}
            onMouseOver={this.onMouseOverCircle}
            onMouseLeave={this.onMouseLeaveCircle}
            onMouseDown={this.onMouseDownCircle}
            onMouseMove={this.onMouseMoveCircle}
            onMouseUp={this.onMouseUpCircle}
          />
        </g>
        {getNodePage()}
      </g>
    )
  }
}


Node.propTypes = {
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  r: PropTypes.number.isRequired,
  selected: PropTypes.bool.isRequired,
  setSelected: PropTypes.func.isRequired,
  setEdgeStartNodeId: PropTypes.func.isRequired,
  setEdgeEndNodeId: PropTypes.func.isRequired,
  resetEdgeEndNodeId: PropTypes.func.isRequired,
  updateCoordinates: PropTypes.func.isRequired,
  updateNodeMouseOver: PropTypes.func.isRequired,
  getNodeData: PropTypes.func.isRequired,
  updateNodeData: PropTypes.func.isRequired
}