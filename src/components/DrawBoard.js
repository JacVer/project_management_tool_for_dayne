import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react'
import * as d3 from 'd3';

import Edge from './Edge'
import Node from './Node'


export default class DrawBoard extends React.Component {
  
  constructor(props) {
    super(props)
    this.svgRef = React.createRef();
    this.fileInputRef = React.createRef()
    this.nodeRadius = 30
    this.arrowHeadWidth = 15
    this.arrowHeadHeight = 20

    this.state = {
      edgeStartNodeId: null,
      edgeEndNodeId: null,
      onMouseMoveCoord: [],
      nodeCount: 0,
      nodes: [],
      edges: []
    }
  }

  componentDidMount() {
    document.addEventListener("keydown", this.onKeyPress)
  }

  onKeyPress = (e) => {
    if(e.keyCode === 46) { // DELETE KEY
      this.removeSelectedNodes()
    }
  }

  // ... Nodes

  getNodeByNodeId = (nodeId) => {
    return this.state.nodes.filter((n) => n.nodeId === nodeId)[0]
  }

  addNode = (x, y) => {
    // TODO : change this to a state modify and not copy
    const newNode = {
      nodeId: this.state.nodeCount, 
      x: x, 
      y: y, 
      r: this.nodeRadius, 
      selected: false, 
      mouseOver: true,
      data: {
        title: "",
        description: ""
      }
    }

    this.setState((state) => ({
      edgeStartNodeId: state.edgeStartNodeId,
      edgeEndNodeId: state.edgeEndNodeId,
      nodeCount: state.nodeCount + 1,
      nodes: state.nodes.concat([newNode]),
      edges: state.edges
    }))
  }

  unSelectNodes = () => {
    this.setState((state) => {
      state.nodes = state.nodes.map(n => {n.selected = false; return n})
      return state
    })
  }

  removeSelectedNodes = () => {
    this.setState((state) => {
      state.nodes = state.nodes.filter(n => !n.selected)
      const nodeIds = state.nodes.map(n => n.nodeId)
      state.edges = state.edges.filter(e => nodeIds.includes(e.startNodeId) && nodeIds.includes(e.endNodeId))
      return state
    })
  }

  selectNode = (nodeId) => () => {
    this.setState((state) => {
      state.nodes = state.nodes.map(n => {
        n.selected = n.nodeId === nodeId
        return n
      })
      return state
    })
  }

  updateNodeCoordinates = (nodeId) => (x, y) => {
    this.setState((state) => {
      state.nodes = state.nodes.map(n => {
        if(n.nodeId === nodeId) {
          n.x = x
          n.y = y
        }
        return n
      })
      return state
    })
  }

  updateNodeMouseOver = (nodeId) => (isMouseOver) => {
    this.setState((state) => {
      state.nodes = state.nodes.map(n => {
        if(n.nodeId === nodeId) {
          n.mouseOver = isMouseOver
        }
        return n
      })
      return state
    })
  }

  getNodeData = (nodeId) => () => {
    const node = this.state.nodes.find(n => n.nodeId === nodeId)
    return node.data
  }

  updateNodeData = (nodeId) => (title, description) => {
    this.setState((state) => {
      state.nodes = state.nodes.map(n => {
        if(n.nodeId === nodeId) {
          n.data.title = title
          n.data.description = description
        }
        return n
      })
      return state
    })
  }

  // ... Edges

  addEdge = (startNodeId, endNodeId) => {
    const edgeAlreadyExists = this.state.edges.some(e => e.startNodeId === startNodeId && e.endNodeId === endNodeId)
    if(!edgeAlreadyExists) {

      const newEdge = {startNodeId: startNodeId, endNodeId: endNodeId, mouseOver: false}

      this.setState((state) => ({
        edgeStartNodeId: state.edgeStartNodeId,
        edgeEndNodeId: state.edgeEndNodeId,
        nodeCount: state.nodeCount,
        nodes: state.nodes,
        edges: state.edges.concat([newEdge])
      }))
    }
  }

  setEdgeStartNodeId = (nodeId) => () => {
    this.setState((state) => {
      state.edgeStartNodeId = nodeId
      return state
    })
  }

  setEdgeEndNodeId = (nodeId) => () => {
    if(this.state.edgeStartNodeId !== null && this.state.edgeStartNodeId !== nodeId && this.state.onMouseMoveCoord.length > 0) {
      this.setState((state) => {
        state.edgeEndNodeId = nodeId
        return state
      })
    }
  }

  resetEdgeEndNodeId =  () => {
    if(this.state.edgeStartNodeId !== null && this.state.edgeEndNodeId !== null && this.state.onMouseMoveCoord.length > 0) {
      this.setState((state) => {
        state.edgeEndNodeId = null
        return state
      })
    }
  }

  updateEdgeMouseOver = (startNodeId, endNodeId) => (mouseOver) => {
    this.setState((state) => {
      state.edges.map(e => {
        if(e.startNodeId === startNodeId && e.endNodeId === endNodeId) {
          e.mouseOver = mouseOver
        }
        return e
      })
      return state
    })
  }

  getPointerCoordOnNodeEdge = (startNodeX, startNodeY, endNodeX, endNodeY, radius) => {
    
    const lineSupport = [endNodeX - startNodeX, endNodeY - startNodeY]
    const getLineAngle = (u1, u2) => {
      if(u1 < 0) return Math.atan(u2 / u1) + Math.PI
      else return Math.atan(u2 / u1)
    }
    
    // ... get line vector angle and the line base (1) in terms of the global base (0)
    const lineAngle = getLineAngle(lineSupport[0], lineSupport[1])
    const x1 = [Math.cos(lineAngle), Math.sin(lineAngle)]

    return [endNodeX - x1[0] * radius, endNodeY - x1[1] * radius]
  }

  

  getDrawnEdge = () => {

    const getEdgePointer = (startNode) => {
      if(this.state.edgeEndNodeId !== null) {
        const endNode = this.getNodeByNodeId(this.state.edgeEndNodeId)
        return this.getPointerCoordOnNodeEdge(startNode.x, startNode.y, endNode.x, endNode.y, this.nodeRadius)
      }
      else {
        return [this.state.onMouseMoveCoord[0], this.state.onMouseMoveCoord[1]]
      }
    }

    if(this.state.edgeStartNodeId !== null && this.state.onMouseMoveCoord.length > 0) {

      const startNode = this.getNodeByNodeId(this.state.edgeStartNodeId)
      const pointerCoord = getEdgePointer(startNode)

      return (
        <Edge
          sourceX={startNode.x}
          sourceY={startNode.y}
          pointerX={pointerCoord[0]}
          pointerY={pointerCoord[1]}
          arrowHeadWidth={this.arrowHeadWidth}
          arrowHeadHeight={this.arrowHeadHeight}
          edgeColor="blue"
          isBeingDrawn={true}
          updateEdgeMouseOver={() => null}
        />
      )
    }
  }

  // ... Event functions

  onClickRect = (e) => {
    this.unSelectNodes(e)
  }

  onDoubleClickRect = (e) => {
    const coord = d3.pointer(e)
    this.addNode(coord[0], coord[1])
  }

  onMouseDownSvg = (e) => {
    this.setState((state) => {
      state.onMouseMoveCoord = []
      return state
    })
  }

  onMouseUpSvg = (e) => {
    if(this.state.edgeStartNodeId !== null && this.state.edgeEndNodeId !== null) {
      this.addEdge(
        this.state.edgeStartNodeId,
        this.state.edgeEndNodeId
      )
    }
    this.setState((state) => {
      state.edgeStartNodeId = null
      state.edgeEndNodeId = null
      state.onMouseMoveCoord = []
      return state
    })
  }

  onMouseMoveSvg = (e) => {
    if (this.state.edgeStartNodeId !== null) {
      const coord = d3.pointer(e)
      this.setState((state) => {
        state.onMouseMoveCoord = coord
        return state
      })
    }
  }

  render () {
    const nodes = this.state.nodes
      .sort((n1, n2) => (n1.selected - n2.selected) * 2 + n1.mouseOver - n2.mouseOver)
      .map((n) => (
        <Node 
          key={n.nodeId} 
          x={n.x} 
          y={n.y} 
          r={n.r} 
          selected={n.selected} 
          setSelected={this.selectNode(n.nodeId)}
          setEdgeStartNodeId={this.setEdgeStartNodeId(n.nodeId)}
          setEdgeEndNodeId={this.setEdgeEndNodeId(n.nodeId)}
          resetEdgeEndNodeId={this.resetEdgeEndNodeId}
          updateCoordinates={this.updateNodeCoordinates(n.nodeId)}
          updateNodeMouseOver={this.updateNodeMouseOver(n.nodeId)}
          getNodeData={this.getNodeData(n.nodeId)}
          updateNodeData={this.updateNodeData(n.nodeId)}
        />
      ))

    const edges = this.state.edges
      .sort((e1, e2) => e1.mouseOver - e2.mouseOver)
      .map((e) => {
        const startNode = this.getNodeByNodeId(e.startNodeId)
        const endNode = this.getNodeByNodeId(e.endNodeId)
        const edgePointer = this.getPointerCoordOnNodeEdge(startNode.x, startNode.y, endNode.x, endNode.y, this.nodeRadius)
        const edgeKey = e.startNodeId + "-" + e.endNodeId

        return (
          <Edge
            key={edgeKey}
            sourceX={startNode.x}
            sourceY={startNode.y}
            pointerX={edgePointer[0]}
            pointerY={edgePointer[1]}
            arrowHeadWidth={this.arrowHeadWidth}
            arrowHeadHeight={this.arrowHeadHeight}
            edgeColor="grey"
            isBeingDrawn={false}
            updateEdgeMouseOver={this.updateEdgeMouseOver(e.startNodeId, e.endNodeId)}
          />
        )
      })

    const getGraphData = () => {

      const nodes = this.state.nodes.map(n => {
        return {
          nodeId: n.nodeId, 
          x: n.x, 
          y: n.y, 
          title: n.title, 
          description: n.description
        }})

      const edges = this.state.edges.map(e => {
        return {
          startNodeId: e.startNodeId,
          endNodeId: e.endNodeId
        }
      })

      return JSON.stringify({
        nodes: nodes,
        edges: edges
      })
    }

    return (
      <div>
          <svg 
          ref={this.svgRef}
          width={this.props.width} 
          height={this.props.height}
          onMouseDown={this.onMouseDownSvg}
          onMouseUp={this.onMouseUpSvg}
          onMouseMove={this.onMouseMoveSvg}
        >
          <rect 
            x={0} 
            y={0} 
            width={this.props.width} 
            height={this.props.height} 
            onClick={this.onClickRect} 
            onDoubleClick={this.onDoubleClickRect} 
            fill="white"
          />
          {edges}
          {this.getDrawnEdge()}
          {nodes}
        </svg>
        <p/>
        <div>
          <input 
            type="file" 
            id="file" 
            ref={this.fileInputRef} 
            style={{display: "none"}}
            onChange={(e) => {
              const reader = new FileReader()
              const fileText = reader.readAsText(e.target.files[0])
              reader.onload = () => {console.log(reader.result)}
            }}
          />
          <Button
            color="brown"
            onClick={() => this.fileInputRef.current.click()}
          >
            Import graph
          </Button>
          <Button 
            color="teal"
            href={"data:text/plain;charset=utf-8," + encodeURIComponent(getGraphData())}
            download="graph.json"
          >
            Export graph
          </Button>
        
        </div>
      </div>
    )
  }
}

DrawBoard.propTypes = {
  height: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired
}